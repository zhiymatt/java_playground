import java.util.*;

public class TestAutoBoxing {
    public static void main(String[] args) {
        // LHS和RHS两边的<Object>都不能忽略
        List<Object> list = new ArrayList<Object>();
        list.add(1);
        // autoboxing出来变成了一个wrapper，不再是primitive type
        System.out.println(list.get(0).getClass());
    }
}
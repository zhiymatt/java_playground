package Test;

public class TestStaticMethod {
    public static void main(String[] args) {
        //！ System.out.println('C'.toLowerCase());
        System.out.println(Character.toLowerCase('C'));
        Character c = 'c';
        //！ System.out.println(c.toLowerCase());
        System.out.println("c".toLowerCase());
        // String的toLowerCase不是static的，可以用instance调用；
        // Character的compareTo、equals、hashCode不是static的，toString有static和non-static的版本，
        // static的方法不能直接"reference."调用
    }
}

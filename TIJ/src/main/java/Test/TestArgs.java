import java.util.*;

public class TestArgs {
    public static void main(String[] args) {
        // index从0开始
        System.out.println(args[0]);
        // 直接print这个String[]，不会打印出内容，只会打印出type和address
        System.out.println(args);
    }
}